'''
Created on 3 sep. 2021

@author: arman
'''

class VectorEspecial:
    #constructor
    def __init__(self, tamano):
        self.__vector = []
        self.__tamano = tamano
    
    def getVector(self):
        return self.__vector
   
    def setVector(self,vector):
        self.__vector = vector
        
    def getTamano(self):
        return self.__tamano
   
    def setTamano(self,tamano):
        self.__tamano = tamano
        
    def llenarVector(self):
        eds = []   
        for i in range(self.getTamano()):
            eds.append(int(input(f"ingresa la calificacion {i+1}: ")))
        self.setVector(eds)
   
    def mostrarVector(self):
        eds = self.getVector()
        print("[", end="")
        for e in eds:
            print(e, end=" ")
        print("]", end="")
        print()
            
    def obtenerPosicionInicio(self):
            return self.getVector()[0]
    
    def obtenerPosicionFin(self):
            return self.getVector()[-1]
    
    def obtenerCantidadElementos(self):
        return len(self.getVector())
    
    def mostrarElementoInicio(self):
        eds = self.getVector()
        print(f"primer elemento: {eds[0]}")
    
    def mostrarElementoFin(self):
        eds = self.getVector()
        print(f"ultimo elemento: {eds[-1]}")
    
    def aumentarTamanoDelArreglo(self, magnitud):
        self.setTamano(self.getTamano()+magnitud)
        eds = self.getVector()
        for i in range(magnitud):
            eds.append(int(0))
        self.setVector(eds)
        print(f"arreglo aumentado correctamente en {magnitud}")
    
    def disminuirTamanoDelArreglo(self, magnitud):
        self.setTamano(self.getTamano()-magnitud)
        eds = self.getVector()
        while magnitud>=len(eds):
            print("no se puede disminuir el arreglo si la magnitud es superior al tamano del arreglo, ingrese la magnitud de nuevo:")
            magnitud=self.validacionEntero()
        
        for i in range(magnitud):
            eds.pop()
        self.setVector(eds)
        print(f"se disminuyo el tamano correctamente en {magnitud}")
        
    def insertarElementoPosicionEspecifica(self, posicion, elemento):
        eds = self.getVector()
        while posicion>len(eds):
            print("la posicion no puede ser superior a la cantidad de elementos del vector, intente de nuevo: ")
            posicion=ve0.validacionEntero()
        
        eds[posicion-1]=elemento
        self.setVector(eds)
        print(f"{elemento} insertado correctamente en {posicion}")
    
    def eliminarElementoPosicionEspecifica(self, posicion):
        eds = self.getVector()
        while posicion>len(eds):
            print("la posicion no puede ser superior a la cantidad de elementos del vector, intente de nuevo: ")
            posicion=ve0.validacionEntero()
        
        eds[posicion-1]=0
        self.setVector(eds)
        print(f"elemento en posicion {posicion} eliminado correctamente")
    
    def invertirElVector(self):
        eds = self.getVector()
        eds=reversed(eds)
        self.setVector(eds)
        print("vector invertido correctamente")
    
    def validacionEntero(self):
        err=1
        while err==1:
            try:
                r = int(input())
            except:
                print("solamente enteros positivos, intente de nuevo: ")
                err=1
            else:
                if r>0:
                    err=0
                else:
                    print("solamente enteros positivos, intente de nuevo: ")
                    err=1
        return r

#==============================PRUEBA=========================

opc=0
ve0 = VectorEspecial(1)
print("tamano del vector: ")
t = ve0.validacionEntero()
ve1 = VectorEspecial(t)

ve1.llenarVector()
ve1.mostrarVector()
while opc!=12:
    print(" 1)obtener posicion inicio \n 2)obtener posicion fin \n 3)obtener cantidad elementos \n 4)mostrar todo los elementos \n 5)mostrar elemento inicio"
                    + " \n 6)mostrar elemento fin \n 7)aumentar tamano del arreglo \n 8)disminuir tamano del arreglo \n 9)insertar elemento posicion especifica "
                    + "\n 10)eliminar elemento posicion especifica \n 11)invertir el vector \n 12)salir")
    
    opc = ve0.validacionEntero()
    if opc<1 or opc>12:
        print("opcion no valida")
    elif opc==1:
        print(ve1.obtenerPosicionInicio())
    elif opc==2:
        print(ve1.obtenerPosicionFin())
    elif opc==3:
        print(ve1.obtenerCantidadElementos())
    elif opc==4:
        ve1.mostrarVector()
    elif opc==5:
        ve1.mostrarElementoInicio()
    elif opc==6:
        ve1.mostrarElementoFin()
    elif opc==7:    
        print("tamano a aumentar: ")
        ve1.aumentarTamanoDelArreglo(ve0.validacionEntero())
    elif opc==8:
        print("tamano a disminuir: ")
        ve1.disminuirTamanoDelArreglo(ve0.validacionEntero())
    elif opc==9:
        print("posicion: ");
        posicion = ve0.validacionEntero();
        print("elemento a insertar: ");
        ve1.insertarElementoPosicionEspecifica(posicion, ve0.validacionEntero());
    elif opc==10:
        print("elemento a eliminar:");
        ve1.eliminarElementoPosicionEspecifica(ve0.validacionEntero());
    elif opc==11:
        ve1.invertirElVector()
print("fin del programa")  
